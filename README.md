# About
A tinker/buff/portal bot plugin.

# Installation:
 - Requires Decal / Virindi View System (comes with virindi bundle)
 - Install [DoThingsBotInstaller-2.0.0.6.exe](/uploads/d15cd820fbb19e85ae22b23479c1b236/DoThingsBotInstaller-2.0.0.6.exe)
    
# How to use:
 - The AC window cannot be minimized while the bot is running.
 - `/config UseCraftSuccessDialog` should be set to on
 - TODO, but hopefully fairly self explanitory.

# Known Issues
 - You must relog the bot after learning spells (from professors at least, untested with scrolls)
 - The bot does not work if the ac window is minimized.  It does not have to be in focus.

# Recipes
- Recipes are stored in <install_dir>/Resources/recipes.xml
- [Supported recipes](https://gitlab.com/trevis/dothingsbot/wikis/recipes)

# Screenshots
![](https://i.gyazo.com/ba7c1b6dedf462864c74d54f4541de73.png)
![](https://i.gyazo.com/2f1cbb53dda27fa07d2ab6b8bf2f53bb.png)

# TODO
 - Calculate success chance on the bot instead of using ingame popup window
 - Tradebot
 - Equipment mana managment
 - Brilliance